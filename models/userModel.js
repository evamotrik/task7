const mongoose = require("mongoose");
const Schema = mongoose.Schema;
    const userScheme = new Schema({
        name: {
            type: String,
            required: true,
            minlength:3,
            maxlength:20
        },
        password: {
            type: String,
            required: true,
            minlength:3,
            maxlength:20
        },
        role: {
            type: String,
            required: true,
            default: "user"
        },
        rooms: [{ type: Schema.Types.ObjectId, ref: 'room' }]
    })

    userScheme.plugin(require('mongoose-autopopulate'));

const User = mongoose.model("user", userScheme);
module.exports = User;
