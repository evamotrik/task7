const mongoose = require("mongoose");
const Schema = mongoose.Schema;

    const roomScheme = new Schema({
        number: {
            type: Number,
            required: true
        },
        users: [{ 
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'user',
            autopopulate: true }],
        admin: { 
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'admin',
            autopopulate: true }
    })

roomScheme.plugin(require('mongoose-autopopulate'));
const Room = mongoose.model('room', roomScheme);
module.exports = Room;