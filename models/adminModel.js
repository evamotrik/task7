const mongoose = require("mongoose");
const Schema = mongoose.Schema;

    const adminScheme = new Schema({
        name: {
            type: String,
            required: true,
            minlength:3,
            maxlength:20
        },
        password: {
            type: String,
            required: true,
            minlength:3,
            maxlength:20
        },
        role: {
            type: String,
            required: true,
            default: 'admin'
        },
        rooms: [{ 
            type: mongoose.Schema.Types.ObjectId, 
            ref: 'room',
            autopopulate: true }]
    })

    adminScheme.plugin(require('mongoose-autopopulate'));
    const Admin = mongoose.model('admin', adminScheme);
module.exports = Admin;