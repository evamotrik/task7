const express = require('express');
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.json());
const userRouter = require('./routes/user.js');
const adminRouter = require('./routes/admin.js');
const fileWorkerRouter = require('./routes/fileWorker');
const authRouter = require('./routes/authRoute');
const roomRouter = require('./routes/roomRoute');
const emitter = require("./emitter.js");
const auth = require("./auth.js");
const fs = require("fs");
app.use("/user", userRouter);
app.use("/admin", adminRouter);
app.use("/fileWorker", fileWorkerRouter);
app.use("/login", authRouter);
app.use("/room", auth.authenticateJWT, roomRouter);

const mongoose = require("mongoose");
mongoose.connect("mongodb://localhost:27017/test", { useUnifiedTopology: true, useNewUrlParser: true });


app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});

emitter.on("writeFile", (postmanFileName,fileName)=>{
    let readableStream = fs.createReadStream(postmanFileName, "utf8");
    let writeableStream = fs.createWriteStream(fileName);
    readableStream.pipe(writeableStream);
});

app.listen(3000);

