const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const userRouter = express.Router();
const userController = require("../controllers/userController.js")

const urlencodedParser = bodyParser.urlencoded({extended: true});

userRouter.post("/create", urlencodedParser, userController.userCreate); 
userRouter.get("/:password", urlencodedParser, userController.userGetPassword);    
userRouter.delete("/:password", urlencodedParser, userController.userDeletePassword);
   

module.exports = userRouter;