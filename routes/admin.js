const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const adminRouter = express.Router();
const adminController = require("../controllers/adminController.js")

const urlencodedParser = bodyParser.urlencoded({extended: true});

adminRouter.post("/create", urlencodedParser, adminController.adminCreate); 
adminRouter.get("/:password", urlencodedParser, adminController.adminGetPassword);    
adminRouter.delete("/:password", urlencodedParser, adminController.adminDeletePassword);
   

module.exports = adminRouter;