const fs = require("fs");
const express = require('express');
const fileWorkerController = require("../controllers/fileWorkerController.js");
const emitter = require("../emitter.js");
const { text } = require("body-parser");
const fileWorkerRouter = express.Router();
const multer = require('multer');
const upload = multer();


fileWorkerRouter.post("/writeSimple", fileWorkerController.writeSimple); 
fileWorkerRouter.post("/readSimple", fileWorkerController.readSimple); 
fileWorkerRouter.post("/writeStream", fileWorkerController.writeStream); 
fileWorkerRouter.post("/readStream", fileWorkerController.readStream); 
fileWorkerRouter.post("/writeFile", upload.single('file'), fileWorkerController.writeFile); 

module.exports = fileWorkerRouter;
