const express = require('express');
const authRouter = express.Router();
const authController = require("../auth.js");
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});

authRouter.post("/",urlencodedParser, authController.checksUser);

module.exports = authRouter;