const express = require('express');
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const roomRouter = express.Router();
const roomController = require("../controllers/roomController.js")
const auth = require("../auth.js")

roomRouter.post("/create", [auth.checkAccess, auth.authenticateJWT], roomController.createRoom); 
roomRouter.delete("/:id", [auth.checkAccess, auth.authenticateJWT], roomController.deleteRoom); 
roomRouter.post("/:id", [auth.checkAccess, auth.authenticateJWT], roomController.changeRoom); 
roomRouter.post("/addUser/:id",  roomController.addUserToRoom); 
roomRouter.get("/roomList", roomController.getsListOfRooms ); 
roomRouter.get("/:id", [auth.checkAccess, auth.authenticateJWT], roomController.getsRoomInfo ); 

module.exports = roomRouter;