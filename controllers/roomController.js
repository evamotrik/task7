const Admin = require('./../models/adminModel.js');
const User = require('./../models/userModel.js');
const Room = require('./../models/roomModel.js');

exports.createRoom = async (request, response) =>{
    let newRoom = new Room({number: request.body.number});
    let roomAdmin = await Admin.findOne({_id : request.user._id});

    newRoom.admin = request.user._id;
    await newRoom.save(); 

    roomAdmin.rooms.push(newRoom._id);
    await roomAdmin.save();
    response.send(`Создана комната с номером: ${newRoom.number}`);
}


exports.addUserToRoom = async(request, response) => {
    let room = await Room.findOne({_id: request.params.id});
    room.users.push(request.user.id);
    await room.save();
    let user = await User.findOne({_id: request.user.id});
    user.rooms.push(request.params.id);
    await user.save();
    response.send(`Пользователь добавлен в комнату`);
}

exports.deleteRoom = async (request, response) => {
    let room = await Room.findOne({_id: request.params.id});
    let roomAdmin = await Admin.findOne({_id: request.user._id});
    if (roomAdmin._id.toString() == room.admin._id.toString()){
        Room.deleteOne({_id: request.params.id}, function(err, result) {
            if (err) return response.send("error");
        });
        for (let i = 0; i < roomAdmin.rooms.length; i++){
            if(roomAdmin.rooms[i]._id.toString() == room._id.toString()){
                roomAdmin.rooms.splice(i,1);
                await roomAdmin.save();
            }
        }
        response.send(`Комната удалена`);  
    } else {
        response.send(`Это не ваша комната. Вы не можете ее удалить`);
    }
}

exports.changeRoom = async (request, response) => {
    let room = await Room.findOne({_id: request.params.id});
    let roomAdmin = await Admin.findOne({_id: request.user._id});
    if (roomAdmin._id.toString() == room.admin._id.toString()){
    Room.updateOne({_id: request.params.id}, {number: request.body.number}, function(err, result){
        response.send(`Номер комнаты изменен на ${request.body.number}`);
    })}
    else {
        response.send(`Это не ваша комната. Вы не можете ее изменить`);
    }
}

exports.getsListOfRooms = async(request, response) => {
    if (request.user.role == "admin") {
        let admin = await Admin.findOne({_id: request.user._id});
        response.send(`Вы создали комнаты с номерами ${admin.rooms}`)
    } else {
        let user = await User.findOne({_id: request.user._id});
        response.send(`Номера комнат в которых вы состоите ${user.rooms}`)
    }    
}

exports.getsRoomInfo = async (request, response) => {
    if (request.user.role == "admin") {
        let room = await Room.findOne({_id: request.params.id});
        response.send(`Info about room ${room}`);
    } else {
        let user = await User.findOne({_id: request.user._id});
        let arr = user.rooms;
        let findRoom = request.params.id;
        arr.includes(findRoom)? response.send(`Room info  ${findRoom}`) : response.send(`Не в этот раз`);
    } 
}



