const User = require('./../models/userModel.js');

exports.userCreate = function(request, response) {
    User.create({name:request.body.name, password:request.body.password}, function(err, doc){    
        if(err) return console.log(err);
        response.send("Сохранен объект user" + doc);
    });
}

exports.userGetPassword = (request, response) => {
    User.find({password:request.params.password}, function(err, doc){
        if(err) return console.log(err);
        response.send("Получен password объекта user" + doc);
    });
}

exports.userDeletePassword = (request, response) => {
    User.deleteOne({password:request.params.password}, function(err, doc){
        if(err) return console.log(err);
        response.send("Удален объект user с password " + doc);
    });
}

