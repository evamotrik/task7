const Admin = require('./../models/adminModel.js');

exports.adminCreate = function(request, response) {
    Admin.create({name:request.body.name, password:request.body.password}, function(err, doc){
          
        if(err) return console.log(err);
          
        console.log("Сохранен объект admin" + doc);
        response.send("Сохранен объект admin" + doc);
    });
}

exports.adminGetPassword = (request, response) => {
    Admin.find({password:request.params.password}, function(err, doc){
        if(err) return console.log(err);
        response.send("Получен id объекта admin" + doc);
    });
}

exports.adminDeletePassword = (request, response) => {
    Admin.deleteOne({password:request.params.password}, function(err, doc){
        if(err) return console.log(err);
        response.send("Удален объект admin с password " + doc);
    });
}