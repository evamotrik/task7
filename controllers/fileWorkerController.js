const { response } = require("express");
const express = require("express")
const router = express.Router()
const fs = require("fs");
const multer = require('multer');
const fileName = "task7text.txt";
const emitter = require("../emitter");
const upload = multer();

exports.writeSimple = () => {
    fs.writeFile("task7text.txt", "Hello world!", function (err) {
        console.log("The file was saved!");
        if (err) throw err;
        console.log('Data has been replaced!');
    });
}

exports.readSimple = () =>{
    fs.readFile('task7text.txt', 'utf8', (err, data) => {
        if (err) throw err;
        console.log('--------- [File Data] ---------');
        console.log(data);
        console.log('--------- [File Data] ---------');
    });
}


exports.writeStream = () => {
    let writeableStream = fs.createWriteStream("task7text.txt");
    console.log("Стрим записан");
    writeableStream.write("Привет мир! \n");
    writeableStream.end("Завершение записи");    
};

exports.readStream = () => {
    let readableStream = fs.createReadStream("task7text.txt", "utf8");
    readableStream.on("data", function(chunk){ 
        console.log("Стрим прочтен");
        console.log(chunk);
    });
}

exports.writeFile = function (request, response) {
       const postmanFileName = request.file.originalname;
       emitter.emit("writeFile", postmanFileName, fileName);
       response.send("write file - done");
}

