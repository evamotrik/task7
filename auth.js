const User = require('./models/userModel');
const Admin = require('./models/adminModel');
const jwt = require('jsonwebtoken');
const accessTokenSecret = 'youraccesstokensecret';

exports.checksUser = async (req, res) => {
  const user = await User.findOne({name: req.body.name, password: req.body.password});
  const admin = await Admin.findOne({name: req.body.name, password: req.body.password});
  if (user) {
          const accessToken = jwt.sign({id: user._id, name: user.name,  role: user.role }, accessTokenSecret, {expiresIn: '30m'});
          res.json({accessToken});
      }else if(admin) {
        const accessToken = jwt.sign({id: admin._id, name: admin.name,  role: admin.role}, accessTokenSecret, {expiresIn: '30m'});
        res.json({accessToken});
      }  
      else {
          res.send('Username or password incorrect');
      }
} 

exports.authenticateJWT = (req, res, next) => {
  const authHeader = req.headers.authorization;

  if(authHeader){
    const token = authHeader.split(' ')[1];
    jwt.verify(token, accessTokenSecret, (err, user) => {
      if(err){
        return res.send("Erroe");
      }
      req.user = user;
      next();
    });
  }
  else{
    res.send("Error");
      }
}

exports.checkAccess = (req, res, next) => {
  if(req.user.role !== "admin"){
    return res.send('You do not have access!');
  }
  next();
}

